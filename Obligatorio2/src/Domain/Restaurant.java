/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.*;

/**
 *
 * @author Gabi
 */
public class Restaurant {
    
    private String name;
    private String address;
    private String schedule;
    private String foodType;
    private ArrayList<Evaluation> evaluationList;

    public Restaurant(String name, String address, String schedule, String foodType) {
        this.name = name;
        this.address = address;
        this.schedule = schedule;
        this.foodType = foodType;
        this.evaluationList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public ArrayList<Evaluation> getEvaluationList() {
        return evaluationList;
    }

    public void setEvaluationList(ArrayList<Evaluation> evaluationList) {
        this.evaluationList = evaluationList;
    }
    

    
    
}
