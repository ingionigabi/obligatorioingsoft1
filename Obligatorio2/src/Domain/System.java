/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.*;

/**
 *
 * @author Gabi
 */
public class System {

    private Restaurant actualRestaurant;
    private ArrayList<Restaurant> restaurantList;

    public System() {
        this.restaurantList = new ArrayList<>();
    }

    public Restaurant getActualRestaurant() {
        return actualRestaurant;
    }

    public void setActualRestaurant(Restaurant actualRestaurant) {
        this.actualRestaurant = actualRestaurant;
    }

    public ArrayList<Restaurant> getRestaurantList() {
        return restaurantList;
    }

    public void setRestaurantList(ArrayList<Restaurant> restaurantList) {
        this.restaurantList = restaurantList;
    }

    public boolean crateRestaurant(String name, String address, String schedule, String foodType) {
        boolean ret = false;

        if (!name.equals("") && !address.equals("") && !schedule.equals("") && !foodType.equals("")) {
            if (!existRestaurant(name)) {
                Restaurant restaurant = new Restaurant(name, address, schedule, foodType);
                restaurantList.add(restaurant);
                ret = true;
            }
        }
        return ret;
    }

    private boolean existRestaurant(String name) {
        for (int i = 0; i < restaurantList.size(); i++) {
            if (restaurantList.get(i).getName().equals(name)) {
                return false;
            }
        }
        return true;
    }
}
