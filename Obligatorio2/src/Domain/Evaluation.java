/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.*;

/**
 *
 * @author Gabi
 */
public class Evaluation {
    private int stars;
    private String review;
    private String name;
    private String email;
    private Date date;

    public Evaluation(int stars, String review, String name, String email) {
        this.stars = stars;
        this.review = review;
        this.name = name;
        this.email = email;
        this.date = new Date();
        
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

   
    
}
