/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.*;

/**
 *
 * @author Gabi
 */
public class Draw {
    private ArrayList<Evaluation> evaluationList;
    private ArrayList<Evaluation> winners;
    private int amount;
    private String prize;
    private Restaurant restaurant;
    private Date date;

    public Draw(int amount, String prize) {
        evaluationList = new ArrayList<>();
        winners = new ArrayList<>();
        date = new Date();
        this.amount = amount;
        this.prize = prize;
    }

    public ArrayList<Evaluation> getEvaluationList() {
        return evaluationList;
    }

    public void setEvaluationList(ArrayList<Evaluation> evaluationList) {
        this.evaluationList = evaluationList;
    }

    public ArrayList<Evaluation> getWinners() {
        return winners;
    }

    public void setWinners(ArrayList<Evaluation> winners) {
        this.winners = winners;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    
    
}
